#include <chrono> // Date and time
#include <functional> // Arithmetic, comparisons, and logical operations
#include <memory> // Dynamic memory management
#include <string> // String functions
#include "eigen3/Eigen/Dense"
#include <vector>
#include "rclcpp/rclcpp.hpp"
#include "ros2_planar_robot/msg/ref_pose.hpp"
#include "ros2_planar_robot/msg/kin_data.hpp"
#include "ros2_planar_robot/srv/data.hpp"
#include <iostream>

using namespace std::chrono_literals;
using std::placeholders::_1;
 
 // High level manager node
class HighLevMng : public rclcpp::Node
{
  public:
    HighLevMng():
      Node("high_lev_mng")
      { 
        pub_next_pose_ = this->create_publisher    <ros2_planar_robot::msg::RefPose>(
            "nxt_pose",10);
        // Subscribe on nxt_pose topic, managed by high_lev_mng node
        subs_pose_   = this->create_subscription <ros2_planar_robot::msg::KinData>   (
            "Kin_data", 10, std::bind(&HighLevMng::topic_callback, this, _1));
       // subs_next_pose_   = this->create_subscription <ros2_planar_robot::msg::RefPose>   (
         //   "launch", 10, std::bind(&TrajGen::topic_callback, this, _1));
        
int numPoses;
auto msg=ros2_planar_robot::msg::RefPose();
std::cout << "Enter the number of reference poses: ";
std::cin >> numPoses; 
//for (int i=0;i<numPoses+1;i=+1)
/*int i=0;
while (i<numPoses)*/
if (i<numPoses){
std::cout << "Enter data for pose " << i + 1 << ": ";
std::cout << "\nx: ";
std::cin >> msg.x;
std::cout << "\ny: ";
std::cin >> msg.y;
std::cout << "\nt: ";
std::cin >> msg.deltat;

stockx.push_back(msg.x);
stocky.push_back(msg.y);
stockt.push_back(msg.deltat);
i=+1;
}
      }


 private:
  void timer_callback()    
  {
    auto message=ros2_planar_robot::msg::RefPose();
    message.x=x_next_ (0);
    message.y=x_next_ (1);
    pub_next_pose_->publish(message);


  }

  void topic_callback(const ros2_planar_robot::msg::KinData::SharedPtr pose )
  {
    double t=0;
    if (t>0){


    }
    else{
    x_previous_ = x_next_;
    x_next_     << pose->pose.x, pose->pose.y ;
    }
    t=t+1;
    
  }
  
 



Eigen::Vector2d x_next_     { Eigen::Vector2d::Zero() } ;
Eigen::Vector2d x_previous_ { Eigen::Vector2d::Zero() } ;
double        t                   { 0                       } ;
std::vector<float> stockx;
std::vector<float> stocky;
std::vector<float> stockt;
rclcpp::TimerBase::SharedPtr                        timer_                                          ;
rclcpp::Publisher<ros2_planar_robot::msg::RefPose>::SharedPtr  pub_next_pose_                               ;   
rclcpp::Subscription<ros2_planar_robot::msg::KinData>::SharedPtr      subs_pose_                                   ;   
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<HighLevMng>());  
  rclcpp::shutdown();
  return 0;
}