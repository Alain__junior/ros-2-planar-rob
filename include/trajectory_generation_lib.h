// Library used for the Trajectory Generation

#pragma once
#include "eigen3/Eigen/Dense"

// 5th degree polynomial
class Polynomial{
  public:
    Polynomial      ();
    Polynomial      (const double &piIn, const double &pfIn, const double & DtIn); 
    void      update(const double &piIn, const double &pfIn, const double & Dt); 
    double p     (const double &t);
    double dp    (const double &t);
  private: 
    std::array<double,6>  a     {   0   };
    double                dt    {   0   };
    double                tb    {   0   };
    double                pi    {   0   };
    double                pf    {   0   };
};

// 2 dimensional point to point trajectory generation
class Point2Point{
  public:
    // Create object
    Point2Point(const Eigen::Vector2d & X_i, const Eigen::Vector2d & X_f, const double & DtIn);
    // Update polynomials
    void update(const Eigen::Vector2d & X_i, const Eigen::Vector2d & X_f, const double & DtIn);
    // Position for given time
    Eigen::Vector2d X (const double & time) ;
    // Velocity for given time
    Eigen::Vector2d dX(const double & time) ;
  private:
    Polynomial      polx                    ;
    Polynomial      poly                    ;
    double          dt          {   0   }   ;
};