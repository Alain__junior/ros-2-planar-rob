// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from ros2_planar_robot:srv/Data.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__STRUCT_H_
#define ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

/// Struct defined in srv/Data in the package ros2_planar_robot.
typedef struct ros2_planar_robot__srv__Data_Request
{
  double x;
  double y;
  double dt;
  double nb;
} ros2_planar_robot__srv__Data_Request;

// Struct for a sequence of ros2_planar_robot__srv__Data_Request.
typedef struct ros2_planar_robot__srv__Data_Request__Sequence
{
  ros2_planar_robot__srv__Data_Request * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} ros2_planar_robot__srv__Data_Request__Sequence;


// Constants defined in the message

/// Struct defined in srv/Data in the package ros2_planar_robot.
typedef struct ros2_planar_robot__srv__Data_Response
{
  uint8_t structure_needs_at_least_one_member;
} ros2_planar_robot__srv__Data_Response;

// Struct for a sequence of ros2_planar_robot__srv__Data_Response.
typedef struct ros2_planar_robot__srv__Data_Response__Sequence
{
  ros2_planar_robot__srv__Data_Response * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} ros2_planar_robot__srv__Data_Response__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__STRUCT_H_
