// generated from rosidl_generator_c/resource/idl__type_support.h.em
// with input from ros2_planar_robot:srv/Data.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__TYPE_SUPPORT_H_
#define ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__TYPE_SUPPORT_H_

#include "rosidl_typesupport_interface/macros.h"

#include "ros2_planar_robot/msg/rosidl_generator_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

#include "rosidl_runtime_c/message_type_support_struct.h"

// Forward declare the get type support functions for this type.
ROSIDL_GENERATOR_C_PUBLIC_ros2_planar_robot
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(
  rosidl_typesupport_c,
  ros2_planar_robot,
  srv,
  Data_Request
)();

// already included above
// #include "rosidl_runtime_c/message_type_support_struct.h"

// Forward declare the get type support functions for this type.
ROSIDL_GENERATOR_C_PUBLIC_ros2_planar_robot
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(
  rosidl_typesupport_c,
  ros2_planar_robot,
  srv,
  Data_Response
)();

#include "rosidl_runtime_c/service_type_support_struct.h"

// Forward declare the get type support functions for this type.
ROSIDL_GENERATOR_C_PUBLIC_ros2_planar_robot
const rosidl_service_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__SERVICE_SYMBOL_NAME(
  rosidl_typesupport_c,
  ros2_planar_robot,
  srv,
  Data
)();

#ifdef __cplusplus
}
#endif

#endif  // ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__TYPE_SUPPORT_H_
