// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from ros2_planar_robot:srv/Data.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__TRAITS_HPP_
#define ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__TRAITS_HPP_

#include <stdint.h>

#include <sstream>
#include <string>
#include <type_traits>

#include "ros2_planar_robot/srv/detail/data__struct.hpp"
#include "rosidl_runtime_cpp/traits.hpp"

namespace ros2_planar_robot
{

namespace srv
{

inline void to_flow_style_yaml(
  const Data_Request & msg,
  std::ostream & out)
{
  out << "{";
  // member: x
  {
    out << "x: ";
    rosidl_generator_traits::value_to_yaml(msg.x, out);
    out << ", ";
  }

  // member: y
  {
    out << "y: ";
    rosidl_generator_traits::value_to_yaml(msg.y, out);
    out << ", ";
  }

  // member: dt
  {
    out << "dt: ";
    rosidl_generator_traits::value_to_yaml(msg.dt, out);
    out << ", ";
  }

  // member: nb
  {
    out << "nb: ";
    rosidl_generator_traits::value_to_yaml(msg.nb, out);
  }
  out << "}";
}  // NOLINT(readability/fn_size)

inline void to_block_style_yaml(
  const Data_Request & msg,
  std::ostream & out, size_t indentation = 0)
{
  // member: x
  {
    if (indentation > 0) {
      out << std::string(indentation, ' ');
    }
    out << "x: ";
    rosidl_generator_traits::value_to_yaml(msg.x, out);
    out << "\n";
  }

  // member: y
  {
    if (indentation > 0) {
      out << std::string(indentation, ' ');
    }
    out << "y: ";
    rosidl_generator_traits::value_to_yaml(msg.y, out);
    out << "\n";
  }

  // member: dt
  {
    if (indentation > 0) {
      out << std::string(indentation, ' ');
    }
    out << "dt: ";
    rosidl_generator_traits::value_to_yaml(msg.dt, out);
    out << "\n";
  }

  // member: nb
  {
    if (indentation > 0) {
      out << std::string(indentation, ' ');
    }
    out << "nb: ";
    rosidl_generator_traits::value_to_yaml(msg.nb, out);
    out << "\n";
  }
}  // NOLINT(readability/fn_size)

inline std::string to_yaml(const Data_Request & msg, bool use_flow_style = false)
{
  std::ostringstream out;
  if (use_flow_style) {
    to_flow_style_yaml(msg, out);
  } else {
    to_block_style_yaml(msg, out);
  }
  return out.str();
}

}  // namespace srv

}  // namespace ros2_planar_robot

namespace rosidl_generator_traits
{

[[deprecated("use ros2_planar_robot::srv::to_block_style_yaml() instead")]]
inline void to_yaml(
  const ros2_planar_robot::srv::Data_Request & msg,
  std::ostream & out, size_t indentation = 0)
{
  ros2_planar_robot::srv::to_block_style_yaml(msg, out, indentation);
}

[[deprecated("use ros2_planar_robot::srv::to_yaml() instead")]]
inline std::string to_yaml(const ros2_planar_robot::srv::Data_Request & msg)
{
  return ros2_planar_robot::srv::to_yaml(msg);
}

template<>
inline const char * data_type<ros2_planar_robot::srv::Data_Request>()
{
  return "ros2_planar_robot::srv::Data_Request";
}

template<>
inline const char * name<ros2_planar_robot::srv::Data_Request>()
{
  return "ros2_planar_robot/srv/Data_Request";
}

template<>
struct has_fixed_size<ros2_planar_robot::srv::Data_Request>
  : std::integral_constant<bool, true> {};

template<>
struct has_bounded_size<ros2_planar_robot::srv::Data_Request>
  : std::integral_constant<bool, true> {};

template<>
struct is_message<ros2_planar_robot::srv::Data_Request>
  : std::true_type {};

}  // namespace rosidl_generator_traits

namespace ros2_planar_robot
{

namespace srv
{

inline void to_flow_style_yaml(
  const Data_Response & msg,
  std::ostream & out)
{
  (void)msg;
  out << "null";
}  // NOLINT(readability/fn_size)

inline void to_block_style_yaml(
  const Data_Response & msg,
  std::ostream & out, size_t indentation = 0)
{
  (void)msg;
  (void)indentation;
  out << "null\n";
}  // NOLINT(readability/fn_size)

inline std::string to_yaml(const Data_Response & msg, bool use_flow_style = false)
{
  std::ostringstream out;
  if (use_flow_style) {
    to_flow_style_yaml(msg, out);
  } else {
    to_block_style_yaml(msg, out);
  }
  return out.str();
}

}  // namespace srv

}  // namespace ros2_planar_robot

namespace rosidl_generator_traits
{

[[deprecated("use ros2_planar_robot::srv::to_block_style_yaml() instead")]]
inline void to_yaml(
  const ros2_planar_robot::srv::Data_Response & msg,
  std::ostream & out, size_t indentation = 0)
{
  ros2_planar_robot::srv::to_block_style_yaml(msg, out, indentation);
}

[[deprecated("use ros2_planar_robot::srv::to_yaml() instead")]]
inline std::string to_yaml(const ros2_planar_robot::srv::Data_Response & msg)
{
  return ros2_planar_robot::srv::to_yaml(msg);
}

template<>
inline const char * data_type<ros2_planar_robot::srv::Data_Response>()
{
  return "ros2_planar_robot::srv::Data_Response";
}

template<>
inline const char * name<ros2_planar_robot::srv::Data_Response>()
{
  return "ros2_planar_robot/srv/Data_Response";
}

template<>
struct has_fixed_size<ros2_planar_robot::srv::Data_Response>
  : std::integral_constant<bool, true> {};

template<>
struct has_bounded_size<ros2_planar_robot::srv::Data_Response>
  : std::integral_constant<bool, true> {};

template<>
struct is_message<ros2_planar_robot::srv::Data_Response>
  : std::true_type {};

}  // namespace rosidl_generator_traits

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<ros2_planar_robot::srv::Data>()
{
  return "ros2_planar_robot::srv::Data";
}

template<>
inline const char * name<ros2_planar_robot::srv::Data>()
{
  return "ros2_planar_robot/srv/Data";
}

template<>
struct has_fixed_size<ros2_planar_robot::srv::Data>
  : std::integral_constant<
    bool,
    has_fixed_size<ros2_planar_robot::srv::Data_Request>::value &&
    has_fixed_size<ros2_planar_robot::srv::Data_Response>::value
  >
{
};

template<>
struct has_bounded_size<ros2_planar_robot::srv::Data>
  : std::integral_constant<
    bool,
    has_bounded_size<ros2_planar_robot::srv::Data_Request>::value &&
    has_bounded_size<ros2_planar_robot::srv::Data_Response>::value
  >
{
};

template<>
struct is_service<ros2_planar_robot::srv::Data>
  : std::true_type
{
};

template<>
struct is_service_request<ros2_planar_robot::srv::Data_Request>
  : std::true_type
{
};

template<>
struct is_service_response<ros2_planar_robot::srv::Data_Response>
  : std::true_type
{
};

}  // namespace rosidl_generator_traits

#endif  // ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__TRAITS_HPP_
