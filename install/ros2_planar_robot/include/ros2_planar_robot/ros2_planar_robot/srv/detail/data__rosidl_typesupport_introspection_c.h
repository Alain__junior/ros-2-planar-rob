// generated from rosidl_typesupport_introspection_c/resource/idl__rosidl_typesupport_introspection_c.h.em
// with input from ros2_planar_robot:srv/Data.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_
#define ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_

#ifdef __cplusplus
extern "C"
{
#endif


#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "ros2_planar_robot/msg/rosidl_typesupport_introspection_c__visibility_control.h"

ROSIDL_TYPESUPPORT_INTROSPECTION_C_PUBLIC_ros2_planar_robot
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ros2_planar_robot, srv, Data_Request)();

// already included above
// #include "rosidl_runtime_c/message_type_support_struct.h"
// already included above
// #include "rosidl_typesupport_interface/macros.h"
// already included above
// #include "ros2_planar_robot/msg/rosidl_typesupport_introspection_c__visibility_control.h"

ROSIDL_TYPESUPPORT_INTROSPECTION_C_PUBLIC_ros2_planar_robot
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ros2_planar_robot, srv, Data_Response)();

#include "rosidl_runtime_c/service_type_support_struct.h"
// already included above
// #include "rosidl_typesupport_interface/macros.h"
// already included above
// #include "ros2_planar_robot/msg/rosidl_typesupport_introspection_c__visibility_control.h"

ROSIDL_TYPESUPPORT_INTROSPECTION_C_PUBLIC_ros2_planar_robot
const rosidl_service_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__SERVICE_SYMBOL_NAME(rosidl_typesupport_introspection_c, ros2_planar_robot, srv, Data)();

#ifdef __cplusplus
}
#endif

#endif  // ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_
