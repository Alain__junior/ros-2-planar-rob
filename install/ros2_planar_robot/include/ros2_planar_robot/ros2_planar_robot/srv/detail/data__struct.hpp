// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from ros2_planar_robot:srv/Data.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__STRUCT_HPP_
#define ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__STRUCT_HPP_

#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>

#include "rosidl_runtime_cpp/bounded_vector.hpp"
#include "rosidl_runtime_cpp/message_initialization.hpp"


#ifndef _WIN32
# define DEPRECATED__ros2_planar_robot__srv__Data_Request __attribute__((deprecated))
#else
# define DEPRECATED__ros2_planar_robot__srv__Data_Request __declspec(deprecated)
#endif

namespace ros2_planar_robot
{

namespace srv
{

// message struct
template<class ContainerAllocator>
struct Data_Request_
{
  using Type = Data_Request_<ContainerAllocator>;

  explicit Data_Request_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->x = 0.0;
      this->y = 0.0;
      this->dt = 0.0;
      this->nb = 0.0;
    }
  }

  explicit Data_Request_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_alloc;
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->x = 0.0;
      this->y = 0.0;
      this->dt = 0.0;
      this->nb = 0.0;
    }
  }

  // field types and members
  using _x_type =
    double;
  _x_type x;
  using _y_type =
    double;
  _y_type y;
  using _dt_type =
    double;
  _dt_type dt;
  using _nb_type =
    double;
  _nb_type nb;

  // setters for named parameter idiom
  Type & set__x(
    const double & _arg)
  {
    this->x = _arg;
    return *this;
  }
  Type & set__y(
    const double & _arg)
  {
    this->y = _arg;
    return *this;
  }
  Type & set__dt(
    const double & _arg)
  {
    this->dt = _arg;
    return *this;
  }
  Type & set__nb(
    const double & _arg)
  {
    this->nb = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    ros2_planar_robot::srv::Data_Request_<ContainerAllocator> *;
  using ConstRawPtr =
    const ros2_planar_robot::srv::Data_Request_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<ros2_planar_robot::srv::Data_Request_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<ros2_planar_robot::srv::Data_Request_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      ros2_planar_robot::srv::Data_Request_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<ros2_planar_robot::srv::Data_Request_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      ros2_planar_robot::srv::Data_Request_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<ros2_planar_robot::srv::Data_Request_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<ros2_planar_robot::srv::Data_Request_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<ros2_planar_robot::srv::Data_Request_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__ros2_planar_robot__srv__Data_Request
    std::shared_ptr<ros2_planar_robot::srv::Data_Request_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__ros2_planar_robot__srv__Data_Request
    std::shared_ptr<ros2_planar_robot::srv::Data_Request_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const Data_Request_ & other) const
  {
    if (this->x != other.x) {
      return false;
    }
    if (this->y != other.y) {
      return false;
    }
    if (this->dt != other.dt) {
      return false;
    }
    if (this->nb != other.nb) {
      return false;
    }
    return true;
  }
  bool operator!=(const Data_Request_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct Data_Request_

// alias to use template instance with default allocator
using Data_Request =
  ros2_planar_robot::srv::Data_Request_<std::allocator<void>>;

// constant definitions

}  // namespace srv

}  // namespace ros2_planar_robot


#ifndef _WIN32
# define DEPRECATED__ros2_planar_robot__srv__Data_Response __attribute__((deprecated))
#else
# define DEPRECATED__ros2_planar_robot__srv__Data_Response __declspec(deprecated)
#endif

namespace ros2_planar_robot
{

namespace srv
{

// message struct
template<class ContainerAllocator>
struct Data_Response_
{
  using Type = Data_Response_<ContainerAllocator>;

  explicit Data_Response_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->structure_needs_at_least_one_member = 0;
    }
  }

  explicit Data_Response_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_alloc;
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->structure_needs_at_least_one_member = 0;
    }
  }

  // field types and members
  using _structure_needs_at_least_one_member_type =
    uint8_t;
  _structure_needs_at_least_one_member_type structure_needs_at_least_one_member;


  // constant declarations

  // pointer types
  using RawPtr =
    ros2_planar_robot::srv::Data_Response_<ContainerAllocator> *;
  using ConstRawPtr =
    const ros2_planar_robot::srv::Data_Response_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<ros2_planar_robot::srv::Data_Response_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<ros2_planar_robot::srv::Data_Response_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      ros2_planar_robot::srv::Data_Response_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<ros2_planar_robot::srv::Data_Response_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      ros2_planar_robot::srv::Data_Response_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<ros2_planar_robot::srv::Data_Response_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<ros2_planar_robot::srv::Data_Response_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<ros2_planar_robot::srv::Data_Response_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__ros2_planar_robot__srv__Data_Response
    std::shared_ptr<ros2_planar_robot::srv::Data_Response_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__ros2_planar_robot__srv__Data_Response
    std::shared_ptr<ros2_planar_robot::srv::Data_Response_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const Data_Response_ & other) const
  {
    if (this->structure_needs_at_least_one_member != other.structure_needs_at_least_one_member) {
      return false;
    }
    return true;
  }
  bool operator!=(const Data_Response_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct Data_Response_

// alias to use template instance with default allocator
using Data_Response =
  ros2_planar_robot::srv::Data_Response_<std::allocator<void>>;

// constant definitions

}  // namespace srv

}  // namespace ros2_planar_robot

namespace ros2_planar_robot
{

namespace srv
{

struct Data
{
  using Request = ros2_planar_robot::srv::Data_Request;
  using Response = ros2_planar_robot::srv::Data_Response;
};

}  // namespace srv

}  // namespace ros2_planar_robot

#endif  // ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__STRUCT_HPP_
