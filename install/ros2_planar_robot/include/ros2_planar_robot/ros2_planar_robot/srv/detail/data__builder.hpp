// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from ros2_planar_robot:srv/Data.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__BUILDER_HPP_
#define ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__BUILDER_HPP_

#include <algorithm>
#include <utility>

#include "ros2_planar_robot/srv/detail/data__struct.hpp"
#include "rosidl_runtime_cpp/message_initialization.hpp"


namespace ros2_planar_robot
{

namespace srv
{

namespace builder
{

class Init_Data_Request_nb
{
public:
  explicit Init_Data_Request_nb(::ros2_planar_robot::srv::Data_Request & msg)
  : msg_(msg)
  {}
  ::ros2_planar_robot::srv::Data_Request nb(::ros2_planar_robot::srv::Data_Request::_nb_type arg)
  {
    msg_.nb = std::move(arg);
    return std::move(msg_);
  }

private:
  ::ros2_planar_robot::srv::Data_Request msg_;
};

class Init_Data_Request_dt
{
public:
  explicit Init_Data_Request_dt(::ros2_planar_robot::srv::Data_Request & msg)
  : msg_(msg)
  {}
  Init_Data_Request_nb dt(::ros2_planar_robot::srv::Data_Request::_dt_type arg)
  {
    msg_.dt = std::move(arg);
    return Init_Data_Request_nb(msg_);
  }

private:
  ::ros2_planar_robot::srv::Data_Request msg_;
};

class Init_Data_Request_y
{
public:
  explicit Init_Data_Request_y(::ros2_planar_robot::srv::Data_Request & msg)
  : msg_(msg)
  {}
  Init_Data_Request_dt y(::ros2_planar_robot::srv::Data_Request::_y_type arg)
  {
    msg_.y = std::move(arg);
    return Init_Data_Request_dt(msg_);
  }

private:
  ::ros2_planar_robot::srv::Data_Request msg_;
};

class Init_Data_Request_x
{
public:
  Init_Data_Request_x()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_Data_Request_y x(::ros2_planar_robot::srv::Data_Request::_x_type arg)
  {
    msg_.x = std::move(arg);
    return Init_Data_Request_y(msg_);
  }

private:
  ::ros2_planar_robot::srv::Data_Request msg_;
};

}  // namespace builder

}  // namespace srv

template<typename MessageType>
auto build();

template<>
inline
auto build<::ros2_planar_robot::srv::Data_Request>()
{
  return ros2_planar_robot::srv::builder::Init_Data_Request_x();
}

}  // namespace ros2_planar_robot


namespace ros2_planar_robot
{

namespace srv
{


}  // namespace srv

template<typename MessageType>
auto build();

template<>
inline
auto build<::ros2_planar_robot::srv::Data_Response>()
{
  return ::ros2_planar_robot::srv::Data_Response(rosidl_runtime_cpp::MessageInitialization::ZERO);
}

}  // namespace ros2_planar_robot

#endif  // ROS2_PLANAR_ROBOT__SRV__DETAIL__DATA__BUILDER_HPP_
