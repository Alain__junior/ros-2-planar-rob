// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from ros2_planar_robot:msg/KinData.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__DETAIL__KIN_DATA__STRUCT_HPP_
#define ROS2_PLANAR_ROBOT__MSG__DETAIL__KIN_DATA__STRUCT_HPP_

#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>

#include "rosidl_runtime_cpp/bounded_vector.hpp"
#include "rosidl_runtime_cpp/message_initialization.hpp"


// Include directives for member types
// Member 'pose'
#include "ros2_planar_robot/msg/detail/pose__struct.hpp"

#ifndef _WIN32
# define DEPRECATED__ros2_planar_robot__msg__KinData __attribute__((deprecated))
#else
# define DEPRECATED__ros2_planar_robot__msg__KinData __declspec(deprecated)
#endif

namespace ros2_planar_robot
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct KinData_
{
  using Type = KinData_<ContainerAllocator>;

  explicit KinData_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  : pose(_init)
  {
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      std::fill<typename std::array<double, 4>::iterator, double>(this->jacobian.begin(), this->jacobian.end(), 0.0);
    }
  }

  explicit KinData_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  : pose(_alloc, _init),
    jacobian(_alloc)
  {
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      std::fill<typename std::array<double, 4>::iterator, double>(this->jacobian.begin(), this->jacobian.end(), 0.0);
    }
  }

  // field types and members
  using _pose_type =
    ros2_planar_robot::msg::Pose_<ContainerAllocator>;
  _pose_type pose;
  using _jacobian_type =
    std::array<double, 4>;
  _jacobian_type jacobian;

  // setters for named parameter idiom
  Type & set__pose(
    const ros2_planar_robot::msg::Pose_<ContainerAllocator> & _arg)
  {
    this->pose = _arg;
    return *this;
  }
  Type & set__jacobian(
    const std::array<double, 4> & _arg)
  {
    this->jacobian = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    ros2_planar_robot::msg::KinData_<ContainerAllocator> *;
  using ConstRawPtr =
    const ros2_planar_robot::msg::KinData_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<ros2_planar_robot::msg::KinData_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<ros2_planar_robot::msg::KinData_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      ros2_planar_robot::msg::KinData_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<ros2_planar_robot::msg::KinData_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      ros2_planar_robot::msg::KinData_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<ros2_planar_robot::msg::KinData_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<ros2_planar_robot::msg::KinData_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<ros2_planar_robot::msg::KinData_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__ros2_planar_robot__msg__KinData
    std::shared_ptr<ros2_planar_robot::msg::KinData_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__ros2_planar_robot__msg__KinData
    std::shared_ptr<ros2_planar_robot::msg::KinData_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const KinData_ & other) const
  {
    if (this->pose != other.pose) {
      return false;
    }
    if (this->jacobian != other.jacobian) {
      return false;
    }
    return true;
  }
  bool operator!=(const KinData_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct KinData_

// alias to use template instance with default allocator
using KinData =
  ros2_planar_robot::msg::KinData_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace ros2_planar_robot

#endif  // ROS2_PLANAR_ROBOT__MSG__DETAIL__KIN_DATA__STRUCT_HPP_
