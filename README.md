# ros2_planar_robot

## November $17^{th}$ 2023

### Develop a `high_lvl_mng` node complying with the following characteristics:
- Subscribed to a topic `/kin_data`, receiving messages with type `KinData`. This type should have the fields:
    - `Pose pose`;
    - `float64[4] jacobian`;

- Subscribed to a topic `/launch`, such that when a `true` value is published, the whole process is launched;

- Publisher in a topic named `/nxt_pose`, in which a reference pose of type `RefPose` is published in the following cases:
    - When a first message from `/kin_data` node is received, in order to initialize the `trajectory_generator`. After that, the manager should wait to the `launch` flag; 
    - When the streamed pose from `/kin_data` node is equal to the last reference pose sent;

- When created, the node should ask the user to enter the number of reference poses, and their data.

- Note that no new pose should be sent when the last `RefPose` is attained.

------------------------------------------------------------------------------------

### Updating your forked directory

You can update your forked repository adding this project as a remote, fetching the updated commits and merging it with your current local:

```
cd ros_workspace/src/ros2_planar_robot
git remote add prof git@gite.lirmm.fr:cavalcanti/ros2_planar_robot.git
git fetch assignment_remote
git merge assignment_remote/main
```
You will probably need to solve the conflicts, selecting the desired changes between your local and this remote. You can use `vscode` for that.

------------------------------------------------------------------------------------

### Answers

#### Discuss the differences between what you achieved in the previous commit and this current commit:

I couldn't implement the `trajectory_generator` because...

#### Write here the instructions to demonstrate the functionality of the developed solutions:

1st terminal:
```
cd ros_workspace
source /opt/ros/...
colcon build
ros2 run ros2_planar_robot ...
```

2nd terminal:
```
ros2 topic echo etc...
```

3rd terminal:
```
ros2 topic pub etc...
```
