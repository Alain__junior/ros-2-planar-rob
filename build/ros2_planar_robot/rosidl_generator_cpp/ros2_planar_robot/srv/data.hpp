// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__SRV__DATA_HPP_
#define ROS2_PLANAR_ROBOT__SRV__DATA_HPP_

#include "ros2_planar_robot/srv/detail/data__struct.hpp"
#include "ros2_planar_robot/srv/detail/data__builder.hpp"
#include "ros2_planar_robot/srv/detail/data__traits.hpp"

#endif  // ROS2_PLANAR_ROBOT__SRV__DATA_HPP_
