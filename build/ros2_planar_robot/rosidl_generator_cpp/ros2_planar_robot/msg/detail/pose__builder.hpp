// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from ros2_planar_robot:msg/Pose.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__DETAIL__POSE__BUILDER_HPP_
#define ROS2_PLANAR_ROBOT__MSG__DETAIL__POSE__BUILDER_HPP_

#include <algorithm>
#include <utility>

#include "ros2_planar_robot/msg/detail/pose__struct.hpp"
#include "rosidl_runtime_cpp/message_initialization.hpp"


namespace ros2_planar_robot
{

namespace msg
{

namespace builder
{

class Init_Pose_y
{
public:
  explicit Init_Pose_y(::ros2_planar_robot::msg::Pose & msg)
  : msg_(msg)
  {}
  ::ros2_planar_robot::msg::Pose y(::ros2_planar_robot::msg::Pose::_y_type arg)
  {
    msg_.y = std::move(arg);
    return std::move(msg_);
  }

private:
  ::ros2_planar_robot::msg::Pose msg_;
};

class Init_Pose_x
{
public:
  Init_Pose_x()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_Pose_y x(::ros2_planar_robot::msg::Pose::_x_type arg)
  {
    msg_.x = std::move(arg);
    return Init_Pose_y(msg_);
  }

private:
  ::ros2_planar_robot::msg::Pose msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::ros2_planar_robot::msg::Pose>()
{
  return ros2_planar_robot::msg::builder::Init_Pose_x();
}

}  // namespace ros2_planar_robot

#endif  // ROS2_PLANAR_ROBOT__MSG__DETAIL__POSE__BUILDER_HPP_
