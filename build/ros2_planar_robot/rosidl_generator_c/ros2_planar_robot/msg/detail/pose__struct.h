// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from ros2_planar_robot:msg/Pose.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__DETAIL__POSE__STRUCT_H_
#define ROS2_PLANAR_ROBOT__MSG__DETAIL__POSE__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

/// Struct defined in msg/Pose in the package ros2_planar_robot.
typedef struct ros2_planar_robot__msg__Pose
{
  double x;
  double y;
} ros2_planar_robot__msg__Pose;

// Struct for a sequence of ros2_planar_robot__msg__Pose.
typedef struct ros2_planar_robot__msg__Pose__Sequence
{
  ros2_planar_robot__msg__Pose * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} ros2_planar_robot__msg__Pose__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // ROS2_PLANAR_ROBOT__MSG__DETAIL__POSE__STRUCT_H_
