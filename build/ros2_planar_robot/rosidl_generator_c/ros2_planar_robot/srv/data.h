// generated from rosidl_generator_c/resource/idl.h.em
// with input from ros2_planar_robot:srv/Data.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__SRV__DATA_H_
#define ROS2_PLANAR_ROBOT__SRV__DATA_H_

#include "ros2_planar_robot/srv/detail/data__struct.h"
#include "ros2_planar_robot/srv/detail/data__functions.h"
#include "ros2_planar_robot/srv/detail/data__type_support.h"

#endif  // ROS2_PLANAR_ROBOT__SRV__DATA_H_
